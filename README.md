<p align="center"><a href="https://laravel.com" target="_blank"><img src="http://countrysidekashmir.com/img/inline-logo.png" width="400"></a></p>

# Countryside Kashmir

Countryside Kashmir is registered with the Department of Tourism, Govt. of Jammu and Kashmir, under registration 172/ADTG. The company is based out of Chandilora Tangmarg, which is also known the Gateway of Gulmarg. The company is a freshly establishment one. With the endeavor of provides exceptional, respectful, trustworthy service to our clients. Honesty and integrity are the sole motives of our company to restore the eroding trust of clients.

## Introduction

This repository is a Laravel 10 Rest API for Countryside Kashmir Website. You can find the documentation <a href="https://documenter.getpostman.com/view/16469909/VUjTm4FU" target="_blank">here</a>.

## Heroku

Each push to the master branch deploys a new version of the <a href="https://countryside-kashmir.herokuapp.com/" target="_blank">app</a> on Heroku. However, due to the termination of free database hosting on Heroku, the app is temporarily unavailable through the link provided above.

## List of commits

You can find a list of commits for this repository <a href="https://github.com/fs98/countryside-kashmir-api/commits/master" target="_blank">here</a>.
